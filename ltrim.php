<?php
//ltrim — Strip whitespace (or other characters) from the beginning of a string
//string ltrim ( string $str [, string $character_mask ] )

$text   = "\t\tThese are a few words :) ...  ";
$binary = "\x09Example string\x0A";
$hello  = "Hello World";

$trimmed =ltrim($text);
var_dump($trimmed);
echo "<br>";
?>