<?php
//str_pad — Pad a string to a certain length with another string
//string str_pad ( string $input , int $pad_length [, string $pad_string = " " [, int $pad_type = STR_PAD_RIGHT ]] )

$input = "Alien";
echo str_pad($input, 9)."<br>";                      // produces "Alien     "
echo str_pad($input, 4,"-=", STR_PAD_LEFT)."<br>";  // produces "-=-=-Alien"
echo str_pad($input, 10, "_", STR_PAD_BOTH)."<br>";   // produces "__Alien___"
echo str_pad($input,  6, "___")."<br>";               // produces "Alien_"
echo str_pad($input,  3, "*")."<br>";                 // produces "Alien"
?>