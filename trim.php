<?php
//trim — Strip whitespace (or other characters) from the beginning and end of a string
//string trim ( string $str [, string $character_mask = " \t\n\r\0\x0B" ] )




$text   = "\t\tThese are a few words :) ...  ";
$binary = "\x09Example string\x0A";
$hello  = "Hello World";

$trimmed = trim($binary);
var_dump($trimmed);
echo "<br>";



?>
