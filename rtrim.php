<?php
//rtrim — Strip whitespace (or other characters) from the end of a string
//string rtrim ( string $str [, string $character_mask ] )

$text   = "\t\tThese are a few words :) ...  ";
$binary = "\x09Example string\x0A";
$hello  = "Hello World";

$trimmed = rtrim($text);
var_dump($trimmed);
echo "<br>";

?>